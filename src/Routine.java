import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Routine extends Serveur implements Runnable {
	String clientSentence;
	String capitalizedSentence;
	Socket connectionSocket;

	InputStream is;
    InputStreamReader isr;
    BufferedReader br;
    
    OutputStream os;
    OutputStreamWriter osw;
    BufferedWriter bw;
	
	InputStream  in;
	public Routine(Socket connection) {
		connectionSocket=connection;
	}
	
	@Override
	public void run(){
		boolean routine;
		 routine= true;
		while(routine) {
			try {
				
				is = connectionSocket.getInputStream();
				isr = new InputStreamReader(is);
				br = new BufferedReader(isr);
				
				os = connectionSocket.getOutputStream();
				osw = new OutputStreamWriter(os);
				bw = new BufferedWriter(osw);
				
				String messageReceive = br.readLine();
                if(messageReceive != null) {
	                System.out.println("Message received from client is "+messageReceive);
	                
	                String returnMessage = messageReceive+"\n";
	                synchronized (listClient) {
		                for(Routine r : listClient) {
		                	System.out.println("Message sent to the client is "+returnMessage);
		                	r.getBufferedWriter().write(returnMessage);
		                	r.getBufferedWriter().flush();
		                }
	                }
                }
			} catch (IOException e){
				routine = false;
				try {
					br.close();
					isr.close();
					is.close();
					
					bw.close();
					osw.close();
					os.close();
					
					synchronized (listClient) {
						listClient.remove(this);
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				System.out.println("Fin de connexion");
				//e.printStackTrace();
			}
		}
	}
	
	public BufferedWriter getBufferedWriter() {
		return this.bw;
	}
}