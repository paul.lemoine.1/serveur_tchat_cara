import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class Serveur {
final static List<Routine> listClient = new ArrayList<>();

	public static void main(String[] args) {
		Socket connectionSocket;
		ServerSocket welcomeSocket;
		
		try {
			
			welcomeSocket = new ServerSocket(6788);
			System.out.println("Initialisation Serveur termin�e...");
			Enumeration e = NetworkInterface.getNetworkInterfaces();
			  while(e.hasMoreElements()) { 
				  NetworkInterface n = (NetworkInterface) e.nextElement(); 
				  Enumeration ee = n.getInetAddresses(); 
				  while(ee.hasMoreElements()) { 
					  InetAddress i = (InetAddress) ee.nextElement();
					  System.out.println(i.getHostAddress()); 
				  } 
			  }
			// System.out.println(welcomeSocket.getInetAddress());
			while (true) {
				connectionSocket = welcomeSocket.accept();
				System.out.println("Nouvelle connexion entrantes..");
				 

				Routine r = new Routine(connectionSocket);
				Thread thread = new Thread(r);
				thread.start();
				synchronized (listClient) {
					listClient.add(r);
				}
				
			}
		} catch (IOException e) {

		}
	}
}
